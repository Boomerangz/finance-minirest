package user

import (
	"net/http"
	// "fmt"

	"gitlab.com/Boomerangz/minirest/app/models"
	"gitlab.com/Boomerangz/minirest/app/daos"
	"gitlab.com/Boomerangz/minirest/app/handlers/common"
	"github.com/kataras/iris"
	"github.com/thedevsaddam/govalidator"
)

func Post(ctx iris.Context) {
	var user models.User

	rules := govalidator.MapData{
		"username": []string{"required"},
	}
	opts := govalidator.Options{
		Request:         ctx.Request(), // request object
		Rules:           rules,         // rules map
		RequiredDefault: true,          // all the field to be pass the rules
		Data:            &user,
	}
	v := govalidator.New(opts)
	formatError := v.ValidateJSON()

	if len(formatError) > 0 {
		common.FormatErrorInterface(formatError, http.StatusBadRequest, ctx)
		return
	}

	userDAO := daos.NewUserDAO()
	err := userDAO.Create(ctx, &user)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	common.FormatResponse(user, http.StatusCreated, ctx)
}
